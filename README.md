# README #

This README explains Team 3's project 1 repository for calculating pi.

### What is this repository for? ###

* This program is set up to calculate the value for pi using the Monte Carlo method. We are throwing darts at a square board with a circle on it. We are then determining how many of the darts fall into the circle in order to calculate pi. 
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

##### Repo owner or admin #####

* Kendra Noneman: kendranoneman@u.boisestate.edu

##### Other community or team contact #####

* John Page: johnpage@u.boisestate.edu
* Matt Lawson: matthewlawson@u.boisestate.edu
* Julie Pipkin: juliepipkin@u.boisestate.edu
* Kody King: kodyking@u.boisestate.edu
* Kiev Dixon: kievdixon@u.boisestate.edu
* Nate Farris: natefarris@u.boisestate.edu

