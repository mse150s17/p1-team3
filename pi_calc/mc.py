#Our Project 1 starter program for calculating pi
import math
import timeit
import numpy
import matplotlib.pyplot as plt

t_start = timeit.default_timer()

#We want to calculate pi using the Monte Carlo Method
#Throw darts at a square dartboard

#Count darts that go in circle vs total number of darts = pi/4
n = 100000 #number of iterations
#n = int(input('Enter the desired amount of iterations: ')) #Line for user interaction
i = 0 #counter for interations
inside = 0 #number of points inside portion to begin

#Defines lists for x and y coordinates
plot_listx = []
plot_listy = []

#Need ability to throw darts at a square
def throw_dart():
	x = numpy.random.random() #each of these should be between 0 and 1
	y = numpy.random.random()
	return x,y
	

#Need ability to determine if the dart is in the circle
def in_circle(point): 
	d = (point[0]**2 + point[1]**2)**(0.5)
	if d > 1.0:
		return 0 #We should change this to a value of 0 or 1 instead of T or F
	return 1 #Same as above

while (i<n): #created a while loop for n interations
	point = throw_dart()
	if in_circle(point) == 1:
		 #Adds points inside the circle to the list of randomly generated points
		plot_listx.append(point[0]) 
		plot_listy.append(point[1])
		inside = inside + 1
	i = i +1
	

ratio = inside/n #find ratio inside vs number of interations

print('Number of interations:', n)
print('Number of hits inside the circle:', inside)
print('Number of hits outside the circle:', i - inside)

pi = ratio * 4 #multiplied by four to give the value for pi
print('Calculated value for pi:', pi)

#Added percentage of error compared to real value of pi
pi_real = 3.1415926535897932384626
error = ((pi-pi_real)/pi_real)*100
print('Percent error from real value for pi:', error,'%')


#Added quick if else statement to quickly gauge how close you were to the value of pi
#if abs(error) > 1.0/10.0: 
#	print('Close... Try using more interations...')
#else:
	#print('You are very close to the actual value of pi!!!')


#Function to display program run time 
time_tot = timeit.default_timer() - t_start
print("Total Runtime:",(round((time_tot),4)),"seconds")

#plots points inside the circle to a graph and shows the graph.
plt.scatter(plot_listx, plot_listy)
#fig = plt.figure()
#fig.canvas.set_window_title('Monte Carlo Pi Approximation')
plt.show() 

#circle = plt.Circle((1,1),1, color='r')
#fig, ax = plt.subplots()
#ax.add_artist(circle)

#x_coordinate = [1]
#y_coordinate = [1]

#plt.plot(x_coordinate, y_coordinate)
#plt.show()


